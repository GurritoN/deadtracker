"""Module for SQLite database work"""
import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from deadtrackerlib.models import DeclarativeBase
from deadtrackerlib.logging import get_logger


def init_db(path=None):
    """
    Initialises database.

    path -- path to database file(string)
    returns session to work with database(sqlalchemy.Session)
    """

    if path is None:
        path = ""
    else:
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

    logger = get_logger()
    logger.info(f"initialising database at {path}")

    engine = create_engine(f"sqlite:///{path}")
    DeclarativeBase.metadata.create_all(engine)
    session = sessionmaker(bind=engine)()

    logger.info("database initialised")

    return session
