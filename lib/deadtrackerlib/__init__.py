"""Is a library for the work of the tracker.

Includes modules:

    operations.py - this module describes the interaction
    with the deadtracker library; describes
    possible interactions between library models.

    Initialization:
    >>> from deadtrackerlib.operations import DTLibOperator
    >>> operator = DTLibOperator(db_path="deadtracker.db", logging_config="logging.cfg")

    Basic work with items in database:
    >>> user = operator.register_user("Vasyan")

    >>> task = operator.create_task(user, "Buy bread")
    >>> subtask = operator.create_task(user, "Earn money")
    >>> operator.add_subtask(user, task, subtask)
    >>> operator.delete_task(task)

    >>> operator.share_task(owner, another_user, task, AccessType.PERMISSION_COMPLETE)

Includes packages:
    logging____
    |logging.py -
    Module includes functions for work with logging;
    init_logger(logging_config=None)
    - creates logger with preset settings; get_logger() - returns
    logger.

    exceptions_____
    |exceptions.py -
    This module is needed for error handling.

    models____
    |models.py -
    This module creates SQLite database modules.

    Class of Notification model.
        This class is used to create notifications.
        Has message to show and time to be shown.

        >>> from deadtrackerlib.models import Notification
        >>> import datetime
        >>> notification = Notification(
        ... message="Hey, listen!",
        ... time=datetime.datetime.now())

    Class of Plan model.
        This class is used to create planners.
        Plan have interval in days.

        >>> from deadtrackerlib.models import Plan
        >>> planner = Plan(
        ... task=Task("Drink medicine"),
        ... period=2,
        ... starting_time=datetime.datetime.now(),
        ... repeats=10)

    Class of task's tag.
        The tag is an identifier for categorizing,
        describing, searching data and specifying
        an internal structure.
        >>> from deadtrackerlib.models import Tag
        >>> tag = Tag(name="name")


    Class of Task model.
        This class is used to create tasks or events.
        Event - task, that cannot be completed before deadline.

        >>> from deadtrackerlib.models import Task
        >>> task = Task(
        ... name="name",
        ... description="desc",
        ... is_event=False,
        ... deadline=datetime.now(),
        ... is_autocomplete=False,
        ... priority=0)

    Class of User model.
        This class is used to create users.
        Stores permissions for tasks.

        >>> from deadtrackerlib.models import User
        >>> user = User("Petya")

    Class of Permission model.
        This class is used to describe permission levels.
        Uses enum AccessType for describing access levels

        >>> from deadtrackerlib.models import Permission
        >>> permission = Permission(user, task, AccessType.PERMISSION_VIEW)

    operations____
    |operations.py -
    This module has operator, that performs operations with database.
    Operator contains interface for all of the library functionality.

    >>> from deadtrackerlib.operations import DTLibOperator
    >>> operator = DTLibOperator(db_path="db/db.db")

    database
    |db.py -
    This module is used to create connection to internal SQLite database.

    Library should be used only via DTLibOperator object for proper work.
"""
from deadtrackerlib.operations import DTLibOperator
