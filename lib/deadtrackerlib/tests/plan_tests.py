import unittest
from datetime import datetime, timedelta
from deadtrackerlib.models import Plan, User, Task


class PlanTests(unittest.TestCase):

    def test_creation(self):
        task = Task("foo")
        Plan(task, period=1, starting_time=datetime.now(), repeats=2)

    def test_updating(self):
        task = Task("foo")
        plan = Plan(task, period=1, starting_time=datetime.now() - timedelta(days=3), repeats=2)
        user = User("Bob")
        user.add_plan(plan)
        self.assertTrue(plan.update_tasks_lazy())
        self.assertEqual(len(user.tasks_permissions), 2)


if __name__ == "__main__":
    unittest.main()
