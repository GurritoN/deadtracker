import unittest
from datetime import date, timedelta, datetime
from deadtrackerlib.operations import DTLibOperator
from deadtrackerlib.exceptions import AppendingError


class LibTests(unittest.TestCase):

    def test_registration_and_deletion(self):
        operator = DTLibOperator()
        operator.register_user("Bob")
        user = operator.get_user("Bob")
        with self.assertRaises(AppendingError):
            operator.register_user("Bob")
        task = operator.create_task(user, "name")
        subtask = operator.create_task(user, "name2")
        operator.add_subtask(user, task, subtask)
        operator.delete_user(user)
        operator.register_user("Bob")

    def test_loading(self):
        operator = DTLibOperator()
        with self.assertRaises(Exception):
            operator.get_user("Bob")
        with self.assertRaises(Exception):
            operator.get_task(1337)

        operator.register_user("Bob")
        operator.get_user("Bob")

        operator.create_task(operator.get_user("Bob"), "name")

        self.assertEqual(operator.get_user("Bob").tasks_permissions[0].task, operator.get_task(1))

    def test_task_creation(self):
        operator = DTLibOperator()
        operator.register_user("Bob")
        user = operator.get_user("Bob")
        operator.create_task(user, "name")

    def test_subtask_work(self):
        operator = DTLibOperator()
        operator.register_user("Bob")
        user = operator.get_user("Bob")
        operator.create_task(user, "name")
        task = operator.get_task(1)
        operator.create_task(user, "name")
        subtask = operator.get_task(2)
        operator.add_subtask(user, task, subtask)
        self.assertEqual(user.tasks_permissions[0].task.blocking_tasks[0], subtask)

    def test_get_tasks(self):
        operator = DTLibOperator()
        user = operator.register_user("Bob")
        operator.create_task(user, "name")
        task1 = operator.get_task(1)
        operator.create_task(user, "name")
        task2 = operator.get_task(2)
        self.assertEqual(
            operator.get_tasks(user, archived=False, starting_time=date.today(), ending_time=date.today() + timedelta(days=1), tags_names=[]),
            [
                task1,
                task2
            ]
        )

    def test_notifications(self):
        operator = DTLibOperator()
        user = operator.register_user("Bob")
        operator.send_notification(user, datetime.now(), "a")
        self.assertEqual(user.active_notifications[0].message, "a")


if __name__ == "__main__":
    unittest.main()
