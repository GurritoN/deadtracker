import unittest
from datetime import datetime
from deadtrackerlib.models import Task, Tag
from deadtrackerlib.exceptions import *


class TaskTests(unittest.TestCase):

    def test_creation_and_deletion(self):
        task = Task("foo")
        event = Task("bar", is_event=True, deadline=datetime.now())
        task.prepare_for_delete()
        event.prepare_for_delete()
        with self.assertRaises(ConflictingConditionsError):
            Task("badfoo", is_event=True)

    def test_linking(self):
        root = Task("foo")
        subtask = Task("bar")

        root.add_subtask(subtask)

        self.assertEqual(root.blocking_tasks[0], subtask)

        with self.assertRaises(LinkError):
            subtask.add_subtask(root)

    def test_completion(self):
        root = Task("foobar", is_autocomplete=True)
        subtask1 = Task("foo")
        subtask2 = Task("bar")

        root.add_subtask(subtask1)
        root.add_subtask(subtask2)

        with self.assertRaises(ConflictingConditionsError):
            root.complete()

        subtask1.complete()
        self.assertFalse(root.is_completed)
        subtask2.complete()
        self.assertTrue(root.is_completed)

        with self.assertRaises(ConflictingConditionsError):
            root.complete()

    def test_copy(self):
        task = Task("foo")
        copy = task.copy()
        self.assertNotEqual(task, copy)

    def test_adding_tag(self):
        task = Task("foo")
        tag1 = Tag("A")
        tag2 = Tag("B")
        task.add_tag(tag1)
        task.add_tag(tag2)
        self.assertEqual(task.tags, [tag1, tag2])
        with self.assertRaises(AppendingError):
            task.add_tag(tag1)


if __name__ == "__main__":
    unittest.main()
