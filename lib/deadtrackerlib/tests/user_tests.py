import unittest
from deadtrackerlib.models import User, Task, AccessType


class UserTests(unittest.TestCase):

    def test_creation_and_deletion(self):
        user = User("Bob")
        task = Task("name", "desc")
        user.add_task_permissions(task, AccessType.PERMISSION_EDIT)
        user.prepare_for_delete()

    def test_task_adding(self):
        user = User("Bob")
        task = Task("name", "desc")
        subtask = Task("name", "desc")
        task.add_subtask(subtask)
        user.add_task_permissions(task, AccessType.PERMISSION_EDIT, recursive=True)
        self.assertEqual(len(user.tasks_permissions), 2)


if __name__ == "__main__":
    unittest.main()
