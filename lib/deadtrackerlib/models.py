"""Contains sqlalchemy orm models and relationships for library work"""

from datetime import (
    datetime,
    timedelta
)
from deadtrackerlib.logging import get_logger
from sqlalchemy import (
    Table,
    Column,
    Integer,
    ForeignKey,
    String,
    DateTime,
    Boolean,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from deadtrackerlib.exceptions import (
    AppendingError,
    LinkError,
    ConflictingConditionsError,
    TimeError,
    ConflictingConditionsError,
    NotContainingError,
)

DeclarativeBase = declarative_base()

task_tag_relationship = Table(
    'task_tag_relationship',
    DeclarativeBase.metadata,
    Column('task_id', Integer, ForeignKey('tasks.id')),
    Column('tag', String, ForeignKey('tags.name'))
)


class AccessType:
    PERMISSION_DENIED = 1
    PERMISSION_VIEW = 2
    PERMISSION_COMPLETE = 3
    PERMISSION_EDIT = 4


class Notification(DeclarativeBase):
    """Notifications model"""
    __tablename__ = "notifications"

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey('users.id'))
    task_id = Column(Integer, ForeignKey('tasks.id'))

    message = Column(String)
    time = Column(DateTime)
    is_viewed = Column(Boolean)

    def __init__(self, time, message):
        self.time = time
        self.message = message
        self.is_viewed = False


class Permission(DeclarativeBase):
    """Permissions model, contains access type"""
    __tablename__ = "permissions"
    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    task_id = Column(Integer, ForeignKey("tasks.id"), primary_key=True)
    access_type = Column(Integer)
    user = relationship("User", back_populates="tasks_permissions")
    task = relationship("Task", back_populates="users_permissions")


class Task(DeclarativeBase):
    """Model that represents any task or event"""
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True)

    name = Column(String)
    description = Column(String)

    created_on = Column(DateTime)
    edited_on = Column(DateTime)
    completed_on = Column(DateTime)
    deadline = Column(DateTime)

    is_completed = Column(Boolean)
    is_event = Column(Boolean)
    is_failed = Column(Boolean)
    is_autocomplete = Column(Boolean)

    priority = Column(Integer)

    users_permissions = relationship("Permission", back_populates="task")
    waiting_notifications = relationship("Notification", backref="task")

    parental_task_id = Column(Integer, ForeignKey('tasks.id'))
    blocking_tasks = relationship(
        "Task",
        backref=backref('parental_task', remote_side=[id]),
        lazy="select"
    )

    tags = relationship(
        "Tag",
        secondary=task_tag_relationship,
        backref="tasks",
        lazy="select"
    )

    def __init__(
            self,
            name,
            description="",
            is_event=False,
            deadline=None,
            is_autocomplete=False,
            priority=0
    ):

        self.name = name
        self.description = description

        self.is_event = is_event
        if is_event and deadline is None:
            raise ConflictingConditionsError("Event must have deadline")
        else:
            self.deadline = deadline

        self.is_completed = False
        self.is_autocomplete = is_autocomplete

        self.priority = priority

        self.is_completed = self.is_failed = False
        self.edited_on = self.created_on = datetime.now()

    def copy(self, recursive=False):
        """
        Creates a copy of self with the same attributes, except completion status and time

        recursive - if True - copies whole hierarchy with root in this task(Bool)
        returns list of newly created tasks with self in the first position
        """
        new_task = Task(
            self.name,
            self.description,
            self.is_event,
            self.deadline,
            self.is_autocomplete,
            self.priority
        )

        new_task.tags = self.tags
        new_tasks_list = [new_task]

        if recursive:
            for subtask in self.blocking_tasks:
                new_subtasks = subtask.copy(recursive)
                new_task.add_subtask(new_subtasks[0])
                new_tasks_list.extend(new_subtasks)
        logger = get_logger()
        logger.debug(f"Copied task {new_task.name}")

        return new_tasks_list

    def prepare_for_delete(self):
        """
        Used only when no executors left, or plan-container is deleted.
        Unlinks task from hierarchy before deleting from database.
        """
        if self.parental_task is not None:
            self.parental_task.blocking_tasks.remove(self)
        for subtask in self.blocking_tasks:
            subtask.parental_task = None

    def _search_in_upper_tasks(self, task):
        """Searches task in parental tasks recursively."""
        logger = get_logger()
        logger.debug(f"Searching task {task.id} in task {self.id}")
        if self.parental_task is not None:
            return self == task or self.parental_task._search_in_upper_tasks(task)
        return self == task

    def add_subtask(self, task):
        """Tries to link task as blocking task to self."""
        if task.parental_task is not None:
            raise ConflictingConditionsError("Task can have only one parent")

        if task in self.blocking_tasks:
            raise AppendingError("Subtask already linked to this task")

        if self._search_in_upper_tasks(task):
            raise LinkError("Cyclic dependency is not allowed")

        self.blocking_tasks.append(task)

        # tries to complete if we added completed task
        if self.is_autocomplete:
            try:
                self.complete()
            except:
                pass

    def remove_subtask(self, task):
        """Unlinks blocking task"""
        if not task in self.blocking_tasks:
            raise LinkError("Tasks not linked")
        self.blocking_tasks.remove(task)

    def complete(self):
        """
        Tries to complete a task.

        If successful - fixes completion time.
        """
        if self.is_failed:
            raise ConflictingConditionsError("Task already failed")
        if self.is_completed:
            raise ConflictingConditionsError("Task already completed")

        if self.is_event and self.deadline > datetime.now():
            raise TimeError("Event can't be completed before deadline")

        is_blocked = False
        for subtask in self.blocking_tasks:
            if not subtask.is_completed:
                is_blocked = True
                break

        if is_blocked:
            raise ConflictingConditionsError("Task is blocked")

        self.is_completed = True
        self.completed_on = datetime.now()

        if self.parental_task is not None and self.parental_task.is_autocomplete:
            try:
                self.parental_task.complete()
            except:
                pass

    def fail(self):
        """
        Tries to fail a task.

        If successful - fixes failing time.
        Automatically fails all upper tasks recursively.
        """
        if self.is_completed:
            raise ConflictingConditionsError("Task already completed")
        if self.is_failed:
            raise ConflictingConditionsError("Task already failed")
        self.is_failed = True
        self.completed_on = datetime.now()
        try:
            if self.parental_task is not None:
                self.parental_task.fail()
        except:
            pass

    def add_tag(self, tag):
        """Add tag, if don't have one yet"""
        if tag in self.tags:
            raise AppendingError(f"Tag {tag.name}")
        self.tags.append(tag)

    def remove_tag(self, tag):
        """Remove tag, if task have it"""
        if tag not in self.tags:
            raise AppendingError(f"Tag {tag.name}")
        self.tags.remove(tag)

    def edit(self, name=None, description=None, deadline=None, priority=None):
        """Edits task and fixes edit time"""
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if deadline is not None:
            self.deadline = deadline
        if priority is not None:
            self.priority = priority
        self.edited_on = datetime.now()


class User(DeclarativeBase):
    """Model that represent a tracker user."""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)

    username = Column(String, unique=True)
    username_low = Column(String, unique=True)

    tasks_permissions = relationship("Permission", back_populates="user")
    plans = relationship("Plan", back_populates="owner")
    active_notifications = relationship("Notification", backref="user")

    def __init__(self, username):
        self.username = username
        self.username_low = username.lower()

    def prepare_for_delete(self):
        """
        Unlinks user from all task before deletion.

        Automatically deletes tasks, if user was the only executor.
        returns list of permissions with tasks, that deleted.
        """
        permissions_deleted = []
        tasks_deleted = []
        for permission in self.tasks_permissions:
            task = permission.task
            permissions_deleted.append(permission)
            if len(task.users_permissions) == 1:
                tasks_deleted.append(task)
                task.prepare_for_delete()
        return permissions_deleted, tasks_deleted

    def add_task_permissions(self, task, access_type=AccessType.PERMISSION_EDIT, recursive=True):
        """
        Gives user rights to work with task

        task -- task to add permissions(Task)
        recursive -- gives rights to whole hierarchy with root in current task(Bool)
        """
        for permission in self.tasks_permissions:
            if permission.task == task:
                raise AppendingError("User already has this task")
        permission = Permission()
        permission.task = task
        permission.access_type = access_type
        permissions = [permission]
        self.tasks_permissions.append(permission)
        if recursive:
            for subtask in task.blocking_tasks:
                try:
                    sub_permissions = self.add_task_permissions(subtask, access_type)
                    permissions.extend(sub_permissions)
                except:
                    pass
        return permissions

    def remove_task_permissions(self, task, recursive=True):
        """
        Removes permissions to edit task from user.

        Automatically deletes tasks, if was the only executor
        task -- task to remove permissions(Task)
        recursive -- for whole hierarchy(Bool)
        returns list of deleted tasks
        """
        task_permission = None
        for permission in self.tasks_permissions:
            if permission.task == task:
                task_permission = permission
                break
        if task_permission is None:
            raise NotContainingError("User don't have permissions for that task")
        permissions_deleted = [task_permission]
        tasks_deleted = []
        if len(task.users_permissions) == 1:
            task.prepare_for_delete()
            tasks_deleted.append(task)
        if recursive:
            for subtask in task.blocking_tasks:
                try:
                    perms, tasks = self.remove_task_permissions(subtask)
                    permissions_deleted.extend(perms)
                    tasks_deleted.extend(tasks)
                except:
                    pass
        return permissions_deleted, tasks_deleted

    def add_plan(self, plan):
        """Adds plan to user"""
        self.plans.append(plan)

    def remove_plan(self, plan):
        """Deletes plan from user"""
        self.plans.remove(plan)

    def get_tasks(
            self,
            archived=False,
            starting_time=datetime.min,
            ending_time=datetime.max,
            tags=[]
    ):
        """
        Returns a list of tasks for current user, that satisfy conditions:

        archived -- tasks already completed or failed.
        starting_time -- start of time interval
        ending_time -- end of time interval
        tags -- tags to search. Task satisfies condition, only if all are present.
        returns tuple (tasklist, new_permissions),
        where tasklist - required tasks, and new_permissions - newly crated permissions.
        """
        tasklist = []
        new_permissions = []
        for plan in self.plans:
            if not plan.is_ended:
                new_permissions.extend(plan.update_tasks_lazy(ending_time))

        for permission in self.tasks_permissions:
            task = permission.task
            # True, if deadline fits in time interval, or task "archived"
            is_fits_requirements = (((task.deadline is None) or (starting_time <= task.deadline < ending_time))
                and ((task.is_completed or task.is_failed) == archived))
            # Checking for tags
            is_all_tags_set = True
            for tag in tags:
                if tag in task.tags:
                    is_all_tags_set = False
                    break
            if is_all_tags_set and is_fits_requirements:
                tasklist.append(task)
        return tasklist, new_permissions

    def check_permissions(self, task):
        """
        Returns permission level for specified task

        task_or_plan -- object to check for
        returns user permissions
        """
        for permission in self.tasks_permissions:
            if permission.task == task:
                return permission.access_type
        return AccessType.PERMISSION_DENIED


class Tag(DeclarativeBase):
    """Model, that represents tag entity."""
    __tablename__ = "tags"

    name = Column(String, primary_key=True)

    def __init__(self, name):
        self.name = name


class Plan(DeclarativeBase):
    """Periodic task creator."""
    __tablename__ = "plans"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    task_id = Column(Integer, ForeignKey("tasks.id"))
    task = relationship("Task")
    owner_id = Column(Integer, ForeignKey('users.id'))
    owner = relationship(
        'User',
        back_populates="plans",
        lazy="select"
    )
    period = Column(Integer)
    repeats = Column(Integer)
    last_date_set = Column(DateTime)

    is_ended = Column(Boolean)

    def __init__(self, task, period, starting_time, repeats):
        self.task = task.copy()[0]
        self.name = self.task.name
        self.last_date_set = starting_time
        self.period = period
        self.repeats = repeats
        self.is_ended = False

    def update_tasks_lazy(self, stop_date=datetime.now()):
        """
        Lazily updates tasks.

        stop_date -- date to stop updating.
        returns flag that shows, ended plan or not.
        """
        logger = get_logger()
        logger.debug(f"Updating plan {self.id}")
        new_permissions = []
        while self.last_date_set < stop_date and (self.repeats is None or self.repeats > 0):
            tasks = self.task.copy()
            self.last_date_set += timedelta(days=self.period)
            for task in tasks:
                task.deadline = self.last_date_set
            task = tasks[0]
            new_permissions.extend(self.owner.add_task_permissions(task))

            if self.repeats is not None:
                self.repeats -= 1
            logger.debug(f"Repeats remaining: {self.repeats}")

        if self.repeats is not None and self.repeats <= 0:
            self.is_ended = True

        return new_permissions
