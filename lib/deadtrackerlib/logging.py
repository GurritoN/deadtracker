import logging
import logging.config

import os

LOGGER_NAME = "deadtrackerlogger"


def get_logger():
    return logging.getLogger(LOGGER_NAME)


def init_logger(
        is_enabled=None,
        log_level=None,
        handler_path=None,
        log_format=None):
    """Gets the logger by name, sets the logging settings: level, file, format, and turns it on or off.

        enabled -- the logger is on or off (bool, default=False)
        log_level -- with what level to log: 'DEBUG','INFO',WARNING','ERROR','CRITICAL' (string, default="DEBUG")
        handler_path -- path to the file in which the logs are written (string, default="./logging/logging.log")
        log_format -- format of the log line (string, default="time - name - [level] - message")
    """

    if is_enabled is None:
        is_enabled = False

    logger = get_logger()
    if not is_enabled:
        logger.disabled = True
        return

    if log_level is None:
        log_level = logging.DEBUG
    if handler_path is None:
        handler_path = "./logging.log"
    if log_format is None:
        log_format = "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s"

    if not os.path.exists(os.path.dirname(handler_path)):
        os.makedirs(os.path.dirname(handler_path))

    if logger.hasHandlers():
        logger.handlers.clear()
    formatter = logging.Formatter(log_format)

    file_handler = logging.FileHandler(handler_path)

    file_handler.setLevel(log_level)
    file_handler.setFormatter(formatter)

    logger.disabled = False
    logger.addHandler(file_handler)
    logger.setLevel(log_level)