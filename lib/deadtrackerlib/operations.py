"""Library operations, the main interface of the library."""

from datetime import datetime
import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from deadtrackerlib.models import (
    User,
    Task,
    Plan,
    Tag,
    AccessType,
    Notification,
)
from deadtrackerlib.exceptions import (
    AppendingError,
    ConflictingConditionsError,
    ConflictingConditionsError,
    TimeError,
    LinkError
)
from deadtrackerlib.db import init_db
from deadtrackerlib.logging import (
    get_logger,
    init_logger
)


class DTLibOperator:

    def __init__(self, db_path=None, is_enabled=None, log_level=None, handler_path=None, log_format=None):
        """Creates new Operator instance with specified database path and logging config"""
        init_logger(is_enabled, log_level, handler_path, log_format)
        self.session = init_db(db_path)

    def get_user(self, username):
        """Returns user with specified username in lower case"""
        logger = get_logger()
        logger.info(f"Loading user {username}")
        return self.session.query(User).filter(User.username_low == username.lower()).one()

    def get_task(self, task_id):
        """Returns task with specified id"""
        logger = get_logger()
        logger.info(f"Loading task with id: {task_id}")
        return self.session.query(Task).filter(Task.id == task_id).one()

    def get_plan(self, plan_id):
        """Returns plan with specified id"""
        logger = get_logger()
        logger.info(f"Loading plan with id: {plan_id}")
        return self.session.query(Plan).filter(Plan.id == plan_id).one()

    def get_notification(self, notification_id):
        """Returns notification with specified id"""
        logger = get_logger()
        logger.info(f"Loading notification with id: {notification_id}")
        return self.session.query(Notification).filter(Notification.id == notification_id).one()

    def register_user(self, username):
        """Creates new user, if user with this username does not exists"""
        logger = get_logger()
        logger.info(f"Registering user {username}")
        try:
            user = User(username)
            self.session.add(user)
            self.session.commit()
            logger.info("User registered")
            return user
        except sqlalchemy.exc.IntegrityError:
            logger.error(f"User {username} already exists")
            self.session.rollback()
            raise AppendingError(f"User {username} already exists")

    def edit_user(self, user, new_username=None, new_password=None, new_email=None):
        """Edits existing user info """
        logger = get_logger()
        logger.info(f"Updating user {user.username} info")
        try:
            self.get_user(new_username)
            logger.error(f"User {new_username} already exists")
            raise AppendingError(f"User {new_username} already exists")
        except NoResultFound:
            if new_username is not None:
                user.username = new_username
                user.username_low = new_username.lower
            if new_password is not None:
                user.password=new_password
            if new_email is not None:
                user.email=new_email
            self.session.commit()
            logger.info("User updated")

    def delete_user(self, user):
        """
        Deletes specified user.

        Can also delete tasks, if user is the only owner.
        """
        logger = get_logger()
        logger.info(f"Deleting user {user.username}")
        permissions_deleted, tasks_deleted = user.prepare_for_delete()
        for notification in user.active_notifications:
            self.session.delete(notification)
        self.session.delete(user)
        for permission in permissions_deleted:
            self.session.delete(permission)
        for task in tasks_deleted:
            self.session.delete(task)
        self.session.commit()
        logger.info("User deleted")

    def create_task(
            self,
            user,
            name,
            description=None,
            is_event=None,
            deadline=None,
            is_autocomplete=None,
            priority=None,
    ):
        """Creates new task and adds notification on deadline"""
        if description is None:
            description = ""
        if is_event is None:
            is_event = False
        if is_autocomplete is None:
            is_autocomplete = False
        if priority is None:
            priority = 0

        logger = get_logger()
        logger.info(f"Creating task {name}")
        task = Task(name, description, is_event, deadline, is_autocomplete, priority)
        permissions = user.add_task_permissions(task, AccessType.PERMISSION_EDIT)
        self.session.add_all(permissions)
        self.session.add(task)
        self.session.commit()
        logger.info("Task created")
        return task

    def share_task(self, caller, user, task, access_type, recursive):
        """
        Gives permissions to work with task to user.

        recursive -- if True, shares the whole hierarchy.
        """
        if access_type == AccessType.PERMISSION_DENIED:
            raise AppendingError("You can't share task and deny access to it")
        logger = get_logger()
        logger.info(f"Trying to add task {task.id} to user {user.username}")
        if caller.check_permissions(task) < access_type:
            logger.error(f"User {caller.username} does not have enough permissions to share this task with that rights")
            raise ConflictingConditionsError(
                f'User {caller.username} does not have enough permissions to share this task')
        try:
            permissions = user.add_task_permissions(task, access_type, recursive)
            self.session.add_all(permissions)
            self.session.commit()
            logger.info("Task added")
        except AppendingError:
            self.session.rollback()
            logger.error("User already had this task")
            raise

    def copy_task(self, user, task, recursive=False):
        """
        Creates a copy of specified task.

        recursive -- copies whole hierarchy.
        """
        logger = get_logger()
        logger.info(f"Copying task {task.id} to user {user.username}")
        new_tasks = task.copy(recursive)
        new_tasks_root = new_tasks[0]
        permissions = user.add_task_permissions(new_tasks_root, AccessType.PERMISSION_EDIT, recursive=True)
        self.session.add_all(new_tasks)
        self.session.add_all(permissions)
        self.session.commit()
        logger.info("Tasks copied")

    def edit_task(
            self,
            caller,
            task,
            name=None,
            description=None,
            deadline=None,
            priority=None
    ):
        """Edits existing task"""
        logger = get_logger()
        logger.info(f"Editing task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_EDIT:
            logger.error(f"User {caller.username} does not have permissions to edit this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to edit this task")
        task.edit(name, description, deadline, priority)
        self.session.commit()
        logger.info("Task edited")

    def delete_task(self, caller, task, recursive=True):
        """
        Deletes specified task from user
        
        caller -- user, who wants to delete task
        task -- task to be deleted
        recursive -- if True, deletes whole hierarchy
        """
        logger = get_logger()
        logger.info(f"Deleting task {task.id}")
        if caller.check_permissions(task) == AccessType.PERMISSION_DENIED:
            logger.error(f"User {caller.username} does not have permissions to delete this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to delete this task")
        permissions_deleted, tasks_deleted = caller.remove_task_permissions(task, recursive)
        for permission in permissions_deleted:
            self.session.delete(permission)
        for task in tasks_deleted:
            for notification in task.waiting_notifications:
                self.session.delete(notification)
            self.session.delete(task)
        self.session.commit()
        logger.info("Task deleted")

    def complete_task(self, caller, task):
        """Completes specified task"""
        logger = get_logger()
        logger.info(f"Completing task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_COMPLETE:
            logger.error(f"User {caller.username} does not have permissions to complete this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to complete this task")
        try:
            task.complete()
            self.session.commit()
            logger.info("Task completed")
        except ConflictingConditionsError:
            logger.error("Task already completed/failed")
            self.session.rollback()
            raise
        except TimeError:
            logger.error("Tried to complete event before deadline")
            self.session.rollback()
            raise

    def fail_task(self, caller, task):
        """Fails specified task"""
        logger = get_logger()
        logger.info(f"Failing task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_COMPLETE:
            logger.error(f"User {caller.username} does not have permissions to fail this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to fail this task")
        try:
            task.fail()
            self.session.commit()
            logger.info("Task failed")
        except ConflictingConditionsError:
            logger.error("Task already completed/failed")
            self.session.rollback()
            raise

    def add_subtask(self, caller, task, subtask):
        """Adds subtask to task."""
        logger = get_logger()
        logger.info(f"Linking tasks {task.id} and {subtask.id}")
        if (caller.check_permissions(task) < AccessType.PERMISSION_EDIT
                and caller.check_permissions(subtask) < AccessType.PERMISSION_EDIT):
            logger.error(f"User {caller.username} does not have permissions to link this tasks")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to link this tasks")
        try:
            task.add_subtask(subtask)
            self.session.commit()
            logger.info("Tasks linked")
        except ConflictingConditionsError:
            self.session.rollback()
            logger.error("Task already had parent")
            raise
        except AppendingError:
            self.session.rollback()
            logger.error("Tasks already linked")
            raise
        except LinkError:
            self.session.rollback()
            logger.error("Tried to build cyclic dependency")
            raise

    def delete_subtask(self, caller, task, subtask):
        """Deletes subtask from task."""
        logger = get_logger()
        logger.info(f"Unlinking tasks {task.id} and {subtask.id}")
        if (caller.check_permissions(task) < AccessType.PERMISSION_EDIT
                and caller.check_permissions(subtask) < AccessType.PERMISSION_EDIT):
            logger.error(f"User {caller.username} does not have permissions to unlink this tasks")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to unlink this tasks")
        try:
            task.remove_subtask(subtask)
            self.session.commit()
            logger.info("Tasks unlinked")
        except LinkError:
            self.session.rollback()
            logger.error("Tasks wasn't linked")
            raise

    def get_tasks(self, user, archived=None, starting_time=None, ending_time=None, tags_names=None):
        """Gets a collection of tasks, satisfying specified filters"""
        if archived is None:
            archived = False
        if starting_time is None:
            starting_time = datetime.min
        if ending_time is None:
            ending_time = datetime.max
        if tags_names is None:
            tags_names = []

        logger = get_logger()
        logger.info(f"Requested tasks from user {user.username}")
        tags = [tag for tag in self.session.query(Task).all() if tag.name in tags_names]
        tasks, new_perms = user.get_tasks(archived, starting_time, ending_time, tags)
        for perm in new_perms:
            self.session.add(perm.task)
        self.session.add_all(new_perms)
        self.session.commit()
        return tasks

    def get_plans(self, user):
        """Get a user's plans list"""
        logger = get_logger()
        logger.info(f"Requested plans from user {user.username}")
        plans = []
        for plan in user.plans:
            if plan.is_ended:
                user.plans.remove(plan)
                self.session.delete(plan)
            else:
                plans.append(plan)
        self.session.commit()
        return plans

    def add_tags(self, caller, task, tags_names):
        """Adds tags to a specified task"""
        logger = get_logger()
        logger.info(f"Adding tags to task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_EDIT:
            logger.error(f"User {caller.username} does not have permissions to add tags to this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to add tags to this task")
        tags = self.session.query(Tag).all()
        for tag_name in tags_names:
            current_tag = None
            for tag in tags:
                if tag.name == tag_name:
                    current_tag = tag
                    break
            if current_tag is None:
                tag = Tag(tag_name)
                task.add_tag(tag)
                self.session.add(tag)
            else:
                try:
                    task.add_tag(current_tag)
                except AppendingError:
                    logger.error("Task already had this tag")
            logger.info("Tag added")
        self.session.commit()

    def remove_tags(self, caller, task, tags_names):
        """Removes tags from a specified task"""
        logger = get_logger()
        logger.info(f"Removing tags to task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_EDIT:
            logger.error(f"User {caller.username} does not have permissions to remove tags from this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to remove tags from this task")
        tags = self.session.query(Tag).all()
        for tag_name in tags_names:
            current_tag = None
            for tag in tags:
                if tag.name == tag_name:
                    current_tag = tag
                    break
            if current_tag is not None:
                try:
                    task.add_tag(current_tag)
                except AppendingError:
                    logger.error("Task already had this tag")
            logger.info("Tag added")
        self.session.commit()

    def create_plan(self, user, task, period, starting_time, repeats):
        """Creates new plan"""
        logger = get_logger()
        logger.info("Creating plan")
        plan = Plan(task, period, starting_time, repeats)
        user.add_plan(plan)
        self.session.add(plan)
        self.session.commit()
        logger.info("Plan created")
        return plan

    def delete_plan(self, caller, plan):
        """Deletes existing plan"""
        logger = get_logger()
        logger.info(f"Deleting plan {plan.id}")
        if plan not in caller.plans:
            logger.error(f"User {caller.username} does not have permissions to delete this plan")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to delete this plan")
        caller.remove_plan(plan)
        self.session.delete(plan)
        self.session.commit()
        logger.info("Plan deleted")

    def add_notification_plan(self, caller, task, time, message):
        """Adds notification plan to a specified task"""
        logger = get_logger()
        logger.info(f"Adding notification to task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_EDIT:
            logger.error(f"User {caller.username} does not have permissions to add notifications to this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to add notifications to this task")
        notification = Notification(time, message)
        task.waiting_notifications.append(notification)
        self.session.add(notification)
        logger.info("Notification added")
        self.session.commit()

    def send_notification(self, user, time, message):
        """Adds a notification to specified user"""
        logger = get_logger()
        logger.info(f"Sending notification to user {user.username}")
        notification = Notification(time, message)
        user.active_notifications.append(notification)
        self.session.add(notification)
        logger.info("Notification sent")
        self.session.commit()

    def remove_notification_plan(self, caller, task, notification):
        """Removes notification plan from a specified task"""
        logger = get_logger()
        logger.info(f"Removing notification from task {task.id}")
        if caller.check_permissions(task) < AccessType.PERMISSION_EDIT:
            logger.error(f"User {caller.username} does not have permissions to remove notifications from this task")
            raise ConflictingConditionsError(f"User {caller.username} does not have permissions to remove notifications from this task")
        if notification not in task.waiting_notifications:
            logger.error(f"Task {task.id} does not have this notification plan")
            raise ConflictingConditionsError(f"Task {task.id} does not have this notification plan")
        self.session.delete(notification)
        logger.info("Notification removed")
        self.session.commit()

    def remove_notification(self, caller, notification):
        """Removes notification from a specified user"""
        logger = get_logger()
        logger.info(f"Removing notification from user {caller.username}")
        if notification not in caller.active_notifications:
            logger.error(f"User {caller.username} does not have this notification")
            raise ConflictingConditionsError(f"User {caller.username} does not have this notification")
        self.session.delete(notification)
        logger.info("Notification removed")
        self.session.commit()

    def update_plan(self, plan, date):
        """Updates plan to specified date"""
        logger = get_logger()
        logger.info(f"Updating plan {plan.id}")
        new_perms = plan.update_tasks_lazy(date)
        for perm in new_perms:
            self.session.add(perm.task)
        self.session.add_all(new_perms)
        logger.info("Plan updated")
        self.session.commit()

    def update_notifications(self, caller):
        """Updates pending notifications"""
        logger = get_logger()
        logger.info(f"Updating notifications for user {caller.username}")
        for task_perm in caller.tasks_permissions:
            for notification in task_perm.task.waiting_notifications:
                if notification.time < datetime.now():
                    for user_perm in task_perm.task.users_permissions:
                        user = user_perm.user
                        self.send_notification(user, notification.time, notification.message)
                    task_perm.task.waiting_notifications.remove(notification)
                    self.session.delete(notification)
        self.session.commit()

    def get_notifications(self, caller):
        """Get list of notifications"""
        self.update_notifications(caller)
        notifications_list = sorted(caller.active_notifications, key=lambda x: x.time, reverse=True)
        for notification in notifications_list:
            notification.new = not notification.is_viewed
            notification.is_viewed = True
        self.session.commit()
        return notifications_list
