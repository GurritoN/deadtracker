class DeadTrackerLibBaseException(Exception):
    """Base exception for all deadtracker library exceptions"""


class LinkError(DeadTrackerLibBaseException):
    """Exception for any links errors."""


class AppendingError(DeadTrackerLibBaseException):
    """Exception for appending to internal collections errors."""


class NotContainingError(DeadTrackerLibBaseException):
    """Exception for deleting something, that does not exist."""


class TimeError(DeadTrackerLibBaseException):
    """Exception for any time-related errors."""


class ConflictingConditionsError(DeadTrackerLibBaseException):
    """Exception for operations with conflicting conditions."""
