from setuptools import setup

setup(
    name='deadtrackerlib',
    version='1.0pre1',
    packages=['deadtrackerlib'],
    include_package_data=True,
    install_requires=['sqlalchemy'],
    url='https://bitbucket.org/GurritoN/deadtracker',
    author='Nikita Gurik',
    author_email='GurritoN@gmail.com',
    description='Deadtracker library',
    test_suite='tests.lib_tests'
)