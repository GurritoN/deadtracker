from setuptools import setup, find_packages

setup(
    name='deadtrackerconsole',
    version='1.0pre1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['deadtrackerlib', 'dateparser'],
    entry_points={
        'console_scripts': [
            'deadtracker=deadtrackerconsole.deadtracker:main',
        ],
    },
    url='https://bitbucket.org/GurritoN/deadtracker',
    author='Nikita Gurik',
    author_email='GurritoN@gmail.com',
    description='Deadtracker library'
)