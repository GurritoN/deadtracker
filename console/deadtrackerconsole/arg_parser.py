import argparse
import dateparser
from deadtrackerlib.models import AccessType
from deadtrackerconsole.operations import DTConsoleOperator


def access_type(value):
    if value == "edit":
        return AccessType.PERMISSION_EDIT
    if value == "view":
        return AccessType.PERMISSION_VIEW
    if value == "complete":
        return AccessType.PERMISSION_COMPLETE
    raise ValueError(f"There is no access type '{value}'")


def parse_args(args=None):
    parser = argparse.ArgumentParser(description="Deadtracker task tracker")
    parser.add_argument("--db", help="Path to the database")
    subparsers = parser.add_subparsers()

    task_parser = subparsers.add_parser("task", help="Operations with tasks")
    task_subparsers = task_parser.add_subparsers()

    create_task_parser = task_subparsers.add_parser("create", help="Create new task")
    create_task_parser.add_argument("name", help="Task name")
    create_task_parser.add_argument("--description", help="Task description")
    create_task_parser.add_argument("--event", action="store_true", help="Create event(requires deadline)")
    create_task_parser.add_argument("--deadline", type=dateparser.parse, help="Task deadline")
    create_task_parser.add_argument("--priority", type=int, default=0, help="Task priority")
    create_task_parser.add_argument(
        "--autocomplete",
        action="store_true",
        help="Task completes after it's subtasks completed")
    create_task_parser.add_argument("--tags", nargs="+", help="Tags")
    create_task_parser.set_defaults(func=DTConsoleOperator.create_task)

    edit_task_parser = task_subparsers.add_parser("edit", help="Edit task")
    edit_task_parser.add_argument("id", help="Task ID")
    edit_task_parser.add_argument("--name", help="Task name")
    edit_task_parser.add_argument("--description", help="Task description")
    edit_task_parser.add_argument("--deadline", type=dateparser.parse, help="Task deadline")
    edit_task_parser.add_argument("--priority", type=int, help="Task priority")

    add_tags_parser = task_subparsers.add_parser("addtags", help="Add tags to task")
    add_tags_parser.add_argument("id", help="Task ID")
    add_tags_parser.add_argument("tags", nargs="+", help="Tags to add")
    add_tags_parser.set_defaults(func=DTConsoleOperator.add_tags)

    remove_tags_parser = task_subparsers.add_parser("removetags", help="Remove tags from task")
    remove_tags_parser.add_argument("id", help="Task ID")
    remove_tags_parser.add_argument("tags", nargs="+", help="Tags to remove")
    remove_tags_parser.set_defaults(func=DTConsoleOperator.remove_tags)

    add_notification_parser = task_subparsers.add_parser("addnotification", help="Add notification plan to task")
    add_notification_parser.add_argument("id", help="Task ID")
    add_notification_parser.add_argument("time", type=dateparser.parse, help="Time to notificate")
    add_notification_parser.add_argument("message", help="Notification message")
    add_notification_parser.set_defaults(func=DTConsoleOperator.add_notification_plan)

    remove_notification_parser = task_subparsers.add_parser("removenotification", help="Remove notification plan from task")
    remove_notification_parser.add_argument("task_id", help="Task ID")
    remove_notification_parser.add_argument("notification_id", help="Notification ID")
    remove_notification_parser.set_defaults(func=DTConsoleOperator.remove_notification_plan)

    complete_task_parser = task_subparsers.add_parser("complete", help="Complete task")
    complete_task_parser.add_argument("id", help="Task ID")
    complete_task_parser.set_defaults(func=DTConsoleOperator.complete_task)

    fail_task_parser = task_subparsers.add_parser("fail", help="Fail task")
    fail_task_parser.add_argument("id", help="Task ID")
    fail_task_parser.set_defaults(func=DTConsoleOperator.fail_task)

    delete_task_parser = task_subparsers.add_parser("delete", help="Delete task")
    delete_task_parser.add_argument("id", help="Task ID")
    delete_task_parser.add_argument("--recursive", action="store_true", help="Delete whole hierarchy")
    delete_task_parser.set_defaults(func=DTConsoleOperator.delete_task)

    add_subtask_parser = task_subparsers.add_parser("addsubtask", help="Add subtask")
    add_subtask_parser.add_argument("task_id", help="Task ID")
    add_subtask_parser.add_argument("subtask_id", help="Subtaskask ID")
    add_subtask_parser.set_defaults(func=DTConsoleOperator.add_subtask)

    delete_subtask_parser = task_subparsers.add_parser("deletesubtask", help="Delete subtask")
    delete_subtask_parser.add_argument("task_id", help="Task ID")
    delete_subtask_parser.add_argument("subtask_id", help="Subtask ID")
    delete_subtask_parser.set_defaults(func=DTConsoleOperator.delete_subtask)

    plan_parser = subparsers.add_parser("plan", help="Operations with plans")
    plan_subparsers = plan_parser.add_subparsers()

    create_plan_parser = plan_subparsers.add_parser("create", help="Create plan")
    create_plan_parser.add_argument("task_id", help="Task ID")
    create_plan_parser.add_argument("period", type=int, help="Period(days)")
    create_plan_parser.add_argument("--starting_time", type=dateparser.parse, help="Time to start scheduling")
    create_plan_parser.add_argument("--repeats", type=int, help="Times to repeat before expiration")
    create_plan_parser.set_defaults(func=DTConsoleOperator.create_plan)

    delete_plan_parser = plan_subparsers.add_parser("delete", help="Delete plan")
    delete_plan_parser.add_argument("id", help="Plan ID")
    delete_plan_parser.set_defaults(func=DTConsoleOperator.delete_plan)

    showtasks_parser = task_subparsers.add_parser("show", help="Get list of tasks")
    showtasks_parser.add_argument("--starting_time", type=dateparser.parse, help="Start of time interval")
    showtasks_parser.add_argument("--ending_time", type=dateparser.parse, help="End of time interval")
    showtasks_parser.add_argument("--archived", action="store_true", help="Shows archived tasks")
    showtasks_parser.add_argument("--tags", nargs="*", help="Tags")
    showtasks_parser.add_argument("--long", "-l", action="store_true", help="Show more info")
    showtasks_parser.set_defaults(func=DTConsoleOperator.show_tasks)

    showplans_parser = plan_subparsers.add_parser("show", help="Get list of plans")
    showplans_parser.set_defaults(func=DTConsoleOperator.show_plans)

    notification_parser = subparsers.add_parser("notification", help="Operations with notifications")
    notification_subparsers = notification_parser.add_subparsers()

    notification_show_parser = notification_subparsers.add_parser("show", help="Show all notifications")
    notification_show_parser.set_defaults(func=DTConsoleOperator.show_notifications)

    notification_show_parser = notification_subparsers.add_parser("remove", help="Remove existing notification")
    notification_show_parser.add_argument("id", type=int, help="Notification ID")
    notification_show_parser.set_defaults(func=DTConsoleOperator.remove_notification)

# Here comes the multi-user commands!

    share_task_parser = task_subparsers.add_parser("share", help="Share task with another user")
    share_task_parser.add_argument("id", help="Task ID")
    share_task_parser.add_argument("username", help="User name")
    share_task_parser.add_argument("access_type", type=access_type, help="Access type(can be: edit, view or complete")
    share_task_parser.add_argument("--recursive", action="store_true", help="Share whole hierarchy")
    share_task_parser.set_defaults(func=DTConsoleOperator.share_task)

    return parser.parse_args(args)
