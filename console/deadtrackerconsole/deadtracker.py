#!/usr/bin/python3

from getpass import getuser

import sys

from deadtrackerconsole.arg_parser import parse_args
from deadtrackerconsole.operations import DTConsoleOperator
from deadtrackerlib.operations import DTLibOperator
from deadtrackerconsole import settings


def main():
    try:
        args = parse_args()
        if args.db is not None:
            settings.DB_PATH = args.db
        lib_operator = DTLibOperator(settings.DB_PATH,
                                     settings.LOGGER_IS_ENABLED,
                                     settings.LOGGER_LOG_LEVEL,
                                     settings.LOGGER_HANDLER_PATH,
                                     settings.LOGGER_LOG_FORMAT)
        operator = DTConsoleOperator(args, lib_operator, getuser())
        args.func(operator)
    except Exception as ex:
        raise


if __name__ == "__main__":
    main()
