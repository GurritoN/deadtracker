import logging
import os


DIRECTORY_APP = os.path.join(os.environ["HOME"], "deadtracker")
DB_PATH = os.path.join(DIRECTORY_APP, "deadtracker.db")
LOGGER_IS_ENABLED = True
LOGGER_LOG_LEVEL = logging.DEBUG
LOGGER_HANDLER_PATH = os.path.join(DIRECTORY_APP, "logging.log")
LOGGER_LOG_FORMAT = "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s"
