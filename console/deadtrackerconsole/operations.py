class DTConsoleOperator:

    def __init__(self, console_args, lib_operator, username):
        self.args = console_args
        self.lib_operator = lib_operator
        try:
            self.user = lib_operator.get_user(username)
        except:
            self.user = lib_operator.register_user(username)

    def create_task(self):
        task = self.lib_operator.create_task(
            self.user,
            self.args.name,
            self.args.description,
            self.args.event,
            self.args.deadline,
            self.args.autocomplete)
        print("Task created".format(task.id))

    def edit_task(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.edit_task(
            self.user,
            task,
            self.args.name,
            self.args.description,
            self.args.deadline,
            self.args.priority)
        print("Task edited")

    def complete_task(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.complete_task(self.user, task)
        print("Task completed")

    def fail_task(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.fail_task(self.user, task)
        print("Task failed")

    def delete_task(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.fail_task(self.user, task, self.args.recursive)
        print("Task deleted")

    def add_subtask(self):
        task = self.lib_operator.get_task(self.args.task_id)
        subtask = self.lib_operator.get_task(self.args.subtask_id)
        self.lib_operator.add_subtask(self.user, task, subtask)
        print("Tasks linked")

    def delete_subtask(self):
        task = self.lib_operator.get_task(self.args.task_id)
        subtask = self.lib_operator.get_task(self.args.subtask_id)
        self.lib_operator.delete_subtask(self.user, task, subtask)
        print("Tasks unlinked")

    def create_plan(self):
        task = self.lib_operator.get_task(self.args.task_id)
        self.lib_operator.create_plan(self.user, task, self.args.period, self.args.starting_time, self.args.repeats)
        print("Plan created")

    def delete_plan(self):
        plan = self.lib_operator.get_plan(self.args.id)
        self.lib_operator.delete_plan(self.user, plan)
        print("Plan deleted")

    def show_tasks(self):
        tasks = self.lib_operator.get_tasks(
            self.user,
            self.args.archived,
            self.args.starting_time,
            self.args.ending_time,
            self.args.tags)
        if self.args.long:
            for task in tasks:
                print("{id:X}: {name}\nDescription: {desc}\nDeadline: {deadline}\nPriority: {priority}\n"
                      "Event: {is_event}\nCompleted: {is_completed}\nFailed: {is_failed}\nCreated on: {created}\n"
                      "Edited on: {edited}\nCompleted on: {completed}\n"
                      .format(id=task.id,
                              name=task.name,
                              desc=task.description,
                              deadline=task.deadline,
                              priority=task.priority,
                              is_event=task.is_event,
                              is_completed=task.is_completed,
                              is_failed=task.is_failed,
                              created=task.created_on,
                              edited=task.edited_on,
                              completed=task.completed_on))
        else:
            for task in tasks:
                print("{id:X}: {name}; Deadline: {deadline}"
                      .format(id=task.id,
                              name=task.name,
                              deadline=task.deadline))

    def show_plans(self):
        plans = self.lib_operator.get_plans(self.user)
        for plan in plans:
            print("{id:X}: Task - {taskname}\n"
                  "Started on: {started}\nRepeats {repeats} time(s)\nPeriod: {period} day(s)"
                  .format(id=plan.task.id,
                          taskname=plan.task.name,
                          started=plan.starting_time,
                          repeats=plan.repeats,
                          period=plan.period))

    def show_notifications(self):
        notification_list = self.lib_operator.get_notifications(self.user)
        for notification in notification_list:
            if notification.new:
                print(f"({notification.id}) {notification.time}: {notification.message} (NEW)")
            else:
                print(f"({notification.id}) {notification.time}: {notification.message}")

    def share_task(self):
        self.lib_operator.share_task(self.user,
                                     self.lib_operator.get_user(self.args.username),
                                     self.lib_operator.get_task(self.args.id),
                                     self.args.recursive)
        print("Task shared")

    def add_tags(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.add_tags(self.user, task, self.args.tags)
        print("Tags added")

    def remove_tags(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.remove_tags(self.user, task, self.args.tags)
        print("Tags removed")

    def add_notification_plan(self):
        task = self.lib_operator.get_task(self.args.id)
        self.lib_operator.add_notification_plan(self.user, task, self.args.time, self.args.message)
        print("Notification added")

    def remove_notification_plan(self):
        task = self.lib_operator.get_task(self.args.task_id)
        self.lib_operator.remove_notification_plan(self.user, task, self.args.notification_id)
        print("Notification removed")

    def remove_notification(self):
        self.lib_operator.remove_notification(self.user, self.args.id)
        print("Notification removed")