import re
from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required
import django.contrib.auth as auth
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from deadtrackerlib.operations import DTLibOperator
from deadtrackerlib.models import AccessType
from django.urls import reverse
import DeadTrackerWeb.settings as settings
from deadtracker.forms import *


def update_user_info(func):
    def update(*args, **kwargs):
        operator = DTLibOperator(settings.DB_PATH)
        user = operator.get_user(args[0].user.username)
        operator.update_notifications(user)
        for plan in user.plans:
            operator.update_plan(plan, datetime.now())
        return func(*args, **kwargs)

    return update


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))
    return render(request, "index.html")


def register(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            user_password = form.cleaned_data.get('password1')
            operator = DTLibOperator(settings.DB_PATH)
            try:
                operator.register_user(username=username)
                user = auth.authenticate(request, username=username, password=user_password)
                auth.login(request, user)
                return HttpResponseRedirect(reverse('index'))
            except:
                return render(request, 'registration.html', {'form': form})
        else:
            return render(request, 'registration.html', {'form': form})
    else:
        form = UserCreationForm()
        return render(request, 'registration.html', {'form': form})


@login_required
@update_user_info
def home(request):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    return render(request, "home.html", {"lib_user": user})


@login_required
@update_user_info
def user(request, username):
    operator = DTLibOperator(settings.DB_PATH)
    try:
        user = operator.get_user(username)
    except:
        raise Http404(f"There is no user {username}")
    return render(request, "user.html", {"lib_user": operator.get_user(request.user.username), "current_user": user})


@login_required
@update_user_info
def delete_user(request, username):
    operator = DTLibOperator(settings.DB_PATH)
    operator.delete_user(operator.get_user(username))
    request.user.delete()
    auth.logout(request)
    return HttpResponseRedirect(reverse("deadtracker:index"))


@login_required
@update_user_info
def tasks(request):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    tasks = operator.get_tasks(user, starting_time=datetime.today(), ending_time=datetime.today() + timedelta(days=1))
    if request.method == 'POST':
        form = FilterTasksForm(request.POST)
        if form.is_valid():
            is_archived = form.cleaned_data.get("archived")
            starting_time = form.cleaned_data.get("starting_time").replace(tzinfo=None)
            ending_time = form.cleaned_data.get("ending_time").replace(tzinfo=None)
            tags = form.cleaned_data.get("tags")
            if tags == "":
                tags = None
            else:
                tags = tags.split(',')
            tasks = operator.get_tasks(user,
                                       is_archived,
                                       starting_time,
                                       ending_time,
                                       tags)
            return render(request, "tasks.html", {"lib_user": user, "tasks": tasks, "form": form})
        else:
            return render(request, "tasks.html", {"lib_user": user, "tasks": tasks, "form": form})
    else:
        form = FilterTasksForm()
        return render(request, "tasks.html", {"lib_user": user, "tasks": tasks, "form": form})


@login_required
@update_user_info
def create_task(request):
    if request.method == 'POST':
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            operator = DTLibOperator(settings.DB_PATH)
            current_user = operator.get_user(request.user.username)
            name = form.cleaned_data.get("name")
            description = form.cleaned_data.get("description")
            is_event = form.cleaned_data.get("is_event")
            deadline = form.cleaned_data.get("deadline")
            is_autocomplete = form.cleaned_data.get("is_autocomplete")
            priority = form.cleaned_data.get("priority")
            task = operator.create_task(
                current_user,
                name,
                description,
                is_event,
                deadline,
                is_autocomplete,
                priority)
            return HttpResponseRedirect(reverse('task', args=[task.id]))
        else:
            return render(request, "create_task.html", {"lib_user": user, "form": form})
    else:
        form = CreateTaskForm()
        return render(request, "create_task.html", {"lib_user": user, "form": form})


@login_required
@update_user_info
def task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    if request.method == "POST":
        try:
            sub_id = int(request.POST["add_id"])
            operator.add_subtask(user, operator.get_task(id), operator.get_task(sub_id))
        except:
            pass
        try:
            sub_id = int(request.POST["delete_id"])
            operator.delete_subtask(user, operator.get_task(id), operator.get_task(sub_id))
        except:
            pass
    try:
        task = operator.get_task(id)
    except:
        raise Http404(f"There is no task {id}")
    if user.check_permissions(task) == AccessType.PERMISSION_DENIED:
        return render(request, "error.html", {"lib_user": user})
    return render(request, "task.html", {"lib_user": user, "task": task, "permission": user.check_permissions(task)})


@login_required
@update_user_info  # sssssssssssssssssssssssssssssssssssssssssssssssss
def complete_task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    task = operator.get_task(id)
    try:
        operator.complete_task(operator.get_user(request.user.username), task)
        return HttpResponseRedirect(reverse('tasks'))
    except:
        return HttpResponseRedirect(reverse('task', args=[id]))


@login_required
@update_user_info
def fail_task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    task = operator.get_task(id)
    try:
        operator.fail_task(operator.get_user(request.user.username), task)
        return HttpResponseRedirect(reverse('tasks'))
    except:
        return HttpResponseRedirect(reverse('task', args=[id]))


@login_required
@update_user_info
def edit_task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    task = operator.get_task(id)
    user = operator.get_user(request.user.username)
    if user.check_permissions(task) == AccessType.PERMISSION_DENIED:
        return render(request, "error.html", {"lib_user": user})
    if request.method == 'POST':
        form = EditTaskForm(task, request.POST)
        if form.is_valid():
            name = form.cleaned_data.get("name")
            description = form.cleaned_data.get("description")
            deadline = form.cleaned_data.get("deadline")
            priority = form.cleaned_data.get("priority")
            operator.edit_task(user, task, name, description,deadline, priority)
            return HttpResponseRedirect(reverse('task', args=[task.id]))
        else:
            return render(request, "edit_task.html", {"lib_user": user, "form": form})
    else:
        form = EditTaskForm(task)
        return render(request, "edit_task.html", {"lib_user": user, "task": task, "form": form})


@login_required
@update_user_info
def delete_task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    try:
        task = operator.get_task(id)
    except:
        raise Http404(f"There is no plan {id}")
    if user.check_permissions(task) == AccessType.PERMISSION_DENIED:
        return render(request, "error.html")
    operator.delete_task(user, task)
    return HttpResponseRedirect(reverse('plans'))


@login_required
@update_user_info
def plans(request):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    plans = operator.get_plans(user)
    return render(request, "plans.html", {"plans": plans, "lib_user": user,})


@login_required
@update_user_info
def create_plan(request):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    tasks = [perm.task for perm in user.tasks_permissions]
    if request.method == 'POST':
        form = CreatePlanForm(tasks, request.POST)
        if form.is_valid():
            period = form.cleaned_data.get("period")
            repeats = form.cleaned_data.get("repeats")
            starting_time = form.cleaned_data.get("starting_time")
            task = operator.get_task(form.cleaned_data.get("task"))
            plan = operator.create_plan(
                user,
                task,
                period,
                starting_time,
                repeats)
            return HttpResponseRedirect(reverse('plan', args=[plan.id]))
        else:
            return render(request, "create_plan.html", {"lib_user": user, "form": form})
    else:
        form = CreatePlanForm(tasklist=tasks)
        return render(request, "create_plan.html", {"lib_user": user, "form": form})


@login_required
@update_user_info
def plan(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    try:
        plan = operator.get_plan(id)
    except:
        raise Http404(f"There is no plan {id}")
    if plan not in user.plans:
        return render(request, "error.html", {"lib_user": user})
    return render(request, "plan.html", {"plan": plan, "lib_user": user})


@login_required
@update_user_info
def delete_plan(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    try:
        plan = operator.get_plan(id)
    except:
        raise Http404(f"There is no plan {id}")
    if plan not in user.plans:
        return render(request, "error.html", {"lib_user": user})
    operator.delete_plan(user, plan)
    return HttpResponseRedirect(reverse('plans'))


@login_required
@update_user_info
def notifications(request):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    notifications = operator.get_notifications(user)
    return render(request, "notifications.html", {"notifications": notifications, "lib_user": user})


@login_required
@update_user_info
def share_task(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    try:
        another_user = operator.get_user(request.POST['username'])
        access_level = int(request.POST['accesslevel'])
    except:
        return HttpResponseRedirect(reverse('task', args=[id]))
    task = operator.get_task(id)
    try:
        operator.share_task(user, another_user, task, access_level, False)
    except:
        return render(request, "error.html", {"lib_user": user})
    return HttpResponseRedirect(reverse('task', args=[id]))


@login_required
@update_user_info
def add_subtask(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    sub_id = int(re.findall('^(\d+): ', request.POST['subtask'])[0])
    try:
        print(request.POST)
        subtask = operator.get_task(sub_id)
    except:
        return HttpResponseRedirect(reverse('task', args=[id]))
    task = operator.get_task(id)
    try:
        operator.add_subtask(user, task, subtask)
    except:
        return render(request, "error.html", {"lib_user": user})
    return HttpResponseRedirect(reverse('task', args=[id]))


@login_required
@update_user_info
def remove_subtask(request, id):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    try:
        subtask = operator.get_task(request.POST['subtask'])
    except:
        return HttpResponseRedirect(reverse('task', args=[id]))
    task = operator.get_task(id)
    try:
        operator.delete_subtask(user, task, subtask)
    except:
        return render(request, "error.html", {"lib_user": user})
    return HttpResponseRedirect(reverse('task', args=[id]))


@login_required
@update_user_info
def send_message(request, username):
    operator = DTLibOperator(settings.DB_PATH)
    user = operator.get_user(request.user.username)
    recipient = operator.get_user(username)
    operator.send_notification(recipient, datetime.now(), f"You received message from {user.username}: " + request.POST['message'])
    return HttpResponseRedirect(reverse('user', args=[username]))
