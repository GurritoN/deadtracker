from django.apps import AppConfig


class DeadtrackerConfig(AppConfig):
    name = 'deadtracker'
