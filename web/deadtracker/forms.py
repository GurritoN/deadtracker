from django import forms
from tempus_dominus.widgets import DateTimePicker


class FilterTasksForm(forms.Form):
    starting_time = forms.DateTimeField(label='From', widget=DateTimePicker())
    ending_time = forms.DateTimeField(label='To', widget=DateTimePicker())
    tags = forms.CharField(label='Tags, separated by comma', required=False)
    archived = forms.BooleanField(label='Show archived tasks', required=False)


class CreateTaskForm(forms.Form):
    name = forms.CharField(label='Name')
    description = forms.CharField(label='Description', required=False)
    is_event = forms.BooleanField(label='Create event', required=False)
    deadline = forms.DateTimeField(label='Deadline', required=False, widget=DateTimePicker())
    is_autocomplete = forms.BooleanField(label='Complete task after all subtask completion', required=False)
    priority = forms.IntegerField(label='Priority', required=False)


class CreatePlanForm(forms.Form):

    def __init__(self, tasklist, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["task"] = forms.ChoiceField(choices=[(task.id, task.name) for task in tasklist], label='Original task')

    period = forms.IntegerField(label='Period(in days)')
    repeats = forms.IntegerField(label='Times to repeat plan')
    starting_time = forms.DateTimeField(label='Time to start', widget=DateTimePicker())


class EditTaskForm(forms.Form):

    def __init__(self, task, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"] = forms.CharField(label='Name', required=False, initial=task.name)
        self.fields["description"] = forms.CharField(label='Description', required=False, initial=task.description)
        self.fields["deadline"] = forms.DateTimeField(label='Deadline', required=False, widget=DateTimePicker(), initial=task.deadline)
        self.fields["priority"] = forms.IntegerField(label='Priority', required=False, initial=task.priority)
