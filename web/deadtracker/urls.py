from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^accounts/profile/$', views.index, name='profile'),

    url(r'^user/(?P<username>[a-zA-Z0-9]+)/message/$', views.send_message, name='send_message'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/delete/$', views.delete_user, name='delete_user'),
    url(r'^user/(?P<username>[a-zA-Z0-9]+)/$', views.user, name='user'),

    url(r'^home/$', views.home, name='home'),

    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'index'}, name='logout'),

    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^task/create/$', views.create_task, name='create_task'),
    url(r'^task/(?P<id>[0-9]+)/complete/$', views.complete_task, name='complete_task'),
    url(r'^task/(?P<id>[0-9]+)/fail/$', views.fail_task, name='fail_task'),
    url(r'^task/(?P<id>[0-9]+)/edit/$', views.edit_task, name='edit_task'),
    url(r'^task/(?P<id>[0-9]+)/delete/$', views.delete_task, name='delete_task'),
    url(r'^task/(?P<id>[0-9]+)/share/$', views.share_task, name='share_task'),
    url(r'^task/(?P<id>[0-9]+)/add_sub/$', views.add_subtask, name='add_subtask'),
    url(r'^task/(?P<id>[0-9]+)/rm_sub/$', views.remove_subtask, name='remove_subtask'),
    url(r'^task/(?P<id>[0-9]+)/$', views.task, name='task'),

    url(r'^plans/$', views.plans, name='plans'),
    url(r'^create_plan/$', views.create_plan, name='create_plan'),
    url(r'^plan/(?P<id>[0-9]+)/$', views.plan, name='plan'),
    url(r'^plan/(?P<id>[0-9]+)/delete/$', views.delete_plan, name='delete_plan'),

    url(r'^notifications/$', views.notifications, name='notifications'),
]
