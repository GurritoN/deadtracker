# DeadTracker: Deadline Tracker #

## Description ##
 
  Name of the application describes it's focus. You will need it to track your deadlines.
  The application is divided into two parts:
  
  1. Library for working with tasks, tags, notifications, task planners, database. After installing the library, you can import it for work:
  ```import deadtrackerlib```
  
  2. Console represents the library support in the console application.
  
## Requirements ##

  You need to install sqlalchemy, dateparser and setuptools for this application to work. This tracker supports python version 3.6
  
## Installation ##
  
 1. If you don't have setuptools: 
 ```$ pip3 install -U pip setuptools ``` 
  
 2. Installing deadtrackerlib (library): 
 ```$ cd lib ```
 ```$ python3 setup.py install ``` 
  
 3. Installing deadtracker (console):
 ```$ cd console ```
 ```$ python3 setup.py install ```
  
## Manual ##
  The most important advice when using deadtracker to get help enter:
  ```$ deadtracker -h``` or ```$ deadtracker --help```
  
  Application template ```deadtracker <object> [<args>]```
  
  Objects: task, notification, plan.
  
  ```$ deadtracker task [command]```
  
  ```$ deadtracker plan [command]```
  
  To see available commands: ```$ deadtracker plan -h```

## Configurations ##

In the console/settings.py you can change the path and name of the database and the logging settings.

 DIRECTORY_APP - application folder
 for storing  the database and logs;

 DB_PATH - database file;

DEFAULT_LOGGING_CONFIG - logging config;